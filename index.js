const path = require("path");
const config = require(path.join(__dirname, "..", "..", ".config"));

function HelloWorld() {
  console.log(config.message);
}

module.exports = HelloWorld;
